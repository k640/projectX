#!/bin/bash
if [ $# -eq 0 ] || [ $1 = "help" ]
  then
    echo '
    Commands:
      init - inits the project 
      stage - runs stage 
      help - list of commands
    '
fi


if [ "$1" == "init" ]
then
    echo "initing the project..."
    if [[ ! -f ./certs/private.pem ]]
    then
        echo "Private RSA key do not exists creating..."
        cd certs
        openssl genrsa -out private.pem 3072
        cd ..
        echo "Private RSA key created"

    fi

    if [[ ! -f ./certs/public.pem ]]
    then
        echo "Public RSA key do not exists creating..."
        cd certs
        openssl rsa -in private.pem -pubout -out public.pem
        cd ..
        echo "Public RSA key created"
    fi

    echo "init completed"
fi




if [ "$1" == "stage" ]
then
   docker-compose -f docker-compose.stage.yml up 
fi